package reqresin;


import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostLoginAsSwaggerTest {
    @Test
    @DisplayName("Вызов метода POST/user. Создание нового аккаунта")
    public void successPutUpdateUser() throws IOException, URISyntaxException, InterruptedException {
        byte[] out = "{\"username\":\"eve\",\"email\":\"eve.holt@reqres.in\",\"password\":\"cityslicka\"}".getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/login"))
                .POST(HttpRequest.BodyPublishers.ofByteArray(out))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

        assertEquals(200, response.statusCode());
    }
}
