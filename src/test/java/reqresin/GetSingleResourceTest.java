package reqresin;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetSingleResourceTest {
    @Test
    @DisplayName("Вызов метода GET /resource. Получить инф-ию о ресурсе")
    public void successGetSingleResource() throws IOException, URISyntaxException, InterruptedException {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/unknown/5"))
                .GET().build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

        assertEquals(200, response.statusCode());

    }
}
