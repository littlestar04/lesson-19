package reqresin;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetSingleUserTest {
    @Test
    @DisplayName("Вызов метода GET /singleUser. Получить инф-ию о пользователе")
    public void successGetSingleUser() throws IOException, URISyntaxException, InterruptedException {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/5"))
                .GET().build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

        assertEquals(200, response.statusCode());
    }
}
